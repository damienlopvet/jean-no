'use client';
import React from 'react'
import { ImSpinner8 } from 'react-icons/im'


const Button = ({ type, onClick, loading, value, classes }) => {
  return (
    <button
	    type={type}
	    onClick={onClick}
	    className={`btn disabled:opacity-75 flex flex-row transition-all duration-150 items-center gap-2 ${classes}`}
	    disabled={loading}>
        {loading && <ImSpinner8 className='animate-spin' />} {value}
      </button>
    )
}

export default Button