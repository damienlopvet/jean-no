#!/bin/bash

# Your script logic here
commit_message="$1"

git add .
git commit -m "$commit_message"
git push