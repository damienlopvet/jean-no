import React from "react";

const Brand = ({screen}) => {
	return (
		<div className={`relative ${screen === 'mobile' ? 'h-[var(--mobile-nav-height)]' : 'h-[var(--desktop-nav-height)]'} flex justify-center items-center`}>
			<div className='text-3xl font-bold whitespace-nowrap block font-licorice font-normal z-[2] font-[#4f6d7a]'>Jean-Noël</div>
			<svg id='visual' viewBox='0 0 300 80' width='255' height='81' className='absolute z-[1] top-0 bottom-0 h-full'>
				<g transform='translate(160 52)'>
					<path
						d='M54.1 -87.7C71.8 -83.4 89.2 -72.5 101.4 -56.7C113.7 -41 120.8 -20.5 112 -5.1C103.2 10.3 78.5 20.7 61.2 27.7C43.9 34.7 34.2 38.5 25.2 45.8C16.3 53.2 8.1 64.1 -1 65.8C-10.2 67.6 -20.3 60.2 -36.9 57.3C-53.5 54.4 -76.6 55.9 -88 47.1C-99.4 38.3 -99.2 19.2 -101.4 -1.2C-103.5 -21.7 -108.1 -43.3 -100.2 -58.3C-92.4 -73.4 -72.2 -81.7 -53.4 -85.4C-34.7 -89 -17.3 -88 0.4 -88.7C18.2 -89.5 36.3 -91.9 54.1 -87.7'
						fill='#ffff'></path>
				</g>
			</svg>
		</div>
	);
};

export default Brand;
