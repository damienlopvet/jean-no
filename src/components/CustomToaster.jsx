import React from "react";
import { Toaster } from "react-hot-toast";
import "@/src/app/globals.css";
const CustomToaster = () => {
  return (
    <Toaster
      toastOptions={{
        position: "bottom-center",
        className:'toast-global' ,
        success: {
            className: 'toast-success toast-global',
            duration: 5000,
          },
          error: {
            className:'toast-error toast-global' ,
            duration: 5000,
          }
      }}
    />
  );
};

export default CustomToaster;