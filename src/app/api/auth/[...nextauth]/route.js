import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";



export const authOptions = {
	providers: [
		CredentialsProvider({
			name: "credentials",
			credentials: {
				password: { label: "Password", type: "password" },
			},

			async authorize(credentials) {
				console.log("authorize hitted", credentials);
				if (!credentials || !credentials.password) {
					console.log("no password?");
					return null;
				}
				if(credentials.password === process.env.NEXTAUTH_PASSWORD){
					const user = {
						id: 1,
						name: "jeannoel",
						
					};
					return user;
				}
				
				
				console.log("user", user);
				
				return user;
			},
		}),
	],

	secret: process.env.NEXTAUTH_SECRET,
	debug: process.env.NODE_ENV === "development",
};

const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };
