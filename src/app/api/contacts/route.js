import { NextResponse } from "next/server";

export async function GET(req) {
	return fetch("https://tidycal.com/api/bookings", {
        headers: {
            'Authorization': `Bearer ${process.env.TIDYCAL_API_KEY}`
        }  
    }).then((response) => response.json()).then((data) => {
        let result = []
       let _arr = data.data
       _arr.forEach((element) => {
        let name = element?.contact?.name
        let email = element?.contact?.email
        let tel = element?.questions[0]?.answer
        let mergedArray = {'name':name, 'email':email, 'tel':tel}
        result.push(mergedArray)
           


       })
        return NextResponse.json(result);}
    
    ).catch((error) => {
        return NextResponse.json(error)
    });

}
