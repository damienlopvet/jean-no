"use client";
import React, { useEffect, useState } from "react";
import Script from "next/script";
import { TbGalaxy } from "react-icons/tb";
import Image from "next/image";
export const dynamic = "force-dynamic";
export default function Home() {
	const [loadSpinner, setLoadSpinner] = useState(true);

	useEffect(() => {
		console.log(document.getElementById("tidycal").children[0].title);
		setTimeout(() => {
			//check if the node with id 'tidycal-embed' has child, if not, reload the page
			if (document.getElementById("tidycal")?.children[0].title !== "TidyCal") {
				window.location.reload();
			}
		}, 500);
	}, []);

	return (
		<div className='bg-gradient-to-l from-primary to-quarnary'>
			<Script
				src='https://asset-tidycal.b-cdn.net/js/embed.js'
				onLoad={() => console.log(`script loaded correctly, tidycal onLoad has been populated`)}
				onReady={() => setLoadSpinner(false)}></Script>

			<div className='flex min-h-screen flex-col items-center justify-between p-2  max-w-[980px] mx-auto sm:py-20 pt-[var(--mobile-nav-height)]'>
				<div id='tidycal' className='z-10 w-full items-center justify-between font-mono text-sm lg:flex'>
					{loadSpinner && (
						<div className='flex w-full justify-center items-center'>
							<Image src='/tidyCalSkeleton.jpg' width='980' height='800' alt='tidycal skeleton' />
						</div>
					)}
					<div className='tidycal-embed' data-path='jean-noel/consultation-stripe' title='hellowolrd'></div>
				</div>
				{loadSpinner && (
					<>
						<div className='absolute inset-0 h-full w-full bg black/20 flex justify-center items-center z-10 backdrop-blur-sm'>
							<TbGalaxy className='w-20 h-20 animate-spin -scale-100' />
						</div>
					</>
				)}
			</div>
		</div>
	);
}
