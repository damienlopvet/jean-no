import "./globals.css";
import Nav from "./Nav";
import Footer from "./Footer";
import { Licorice, Source_Sans_3, Caveat } from "next/font/google";
import Provider from "@/src/components/Provider";
import CustomToaster from "@/src/components/CustomToaster";


const licorice = Licorice({weight: ['400'], subsets: ["latin"], variable: "--font-licorice" });
const sans = Source_Sans_3({ subsets: ["latin"], variable: "--font-sans" });
const caveat = Caveat({ subsets: ["latin"], variable: "--font-caveat" });
export const metadata = {

  title: "Jean-Noël, Medium de Père en fils",
  description: "Jean-Noël, Medium de Père en fils",
};

export default function RootLayout({ children }) {
  return (
		<html lang='en'>
			<Provider>
				<body className={`${licorice.variable} ${sans.variable} ${caveat.variable}`}>
					<main className=''>
						<Nav />
						{children}		
						<Footer />
						<CustomToaster />
					</main>
				</body>
			</Provider>
		</html>
  );
}
