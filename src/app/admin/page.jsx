"use client";
import React, { useState, useEffect } from "react";
import Link from "next/link";
import { signIn, signOut } from "next-auth/react";
import { Table, TableBody, TableCaption, TableCell, TableFooter, TableHead, TableHeader, TableRow } from "@/src/components/ui/table";
import { HiOutlineEyeSlash, HiOutlineEye } from "react-icons/hi2";
import Button from "../../../utils/Button";
import toast from "react-hot-toast";
import { useSession } from "next-auth/react";

const SignIn = () => {
	const [user, setUser] = useState("");
	const [visible, setVisible] = useState(false);
	const [signInLoadingButton, setSignInLoadingButton] = useState(false);

	const handleSubmit = async (event) => {
		event.preventDefault();
		setSignInLoadingButton(true);
		signIn("credentials", {
			...user,
			redirect: false,
		})
			.then((res) => {
				if (res && res.error) {
					toast.error("Identifiants incorrects");
				} else {
					toast.success("vous êtes bien connecté");
				}
			})
			.catch((err) => {
				console.log(err);
				toast.error("Une erreur est survenue");
			})
			.finally(() => {
				setSignInLoadingButton(false);
			});
	};

	return (
		<>
			<h2 className='text-xl font-semibold'>Se connecter</h2>
			<form className='flex flex-col gap-2 w-full justify-center items-center text-blue-400 my-4' onSubmit={handleSubmit}>
				<div className='relative flex flex-col w-full'>
					<label htmlFor='password'>Mot de passe</label>
					<input
						id='password'
						type={visible ? "text" : "password"}
						placeholder='Password'
						required
						className='text-black input w-full sm:min-w-[400px]'
						value={user.password}
						onChange={(event) => setUser({ ...user, password: event.target.value })}
					/>
					<button
						type='button'
						aria-label='montrer les caracteres'
						className='absolute p-3 bottom-0 right-0'
						onClick={() => {
							setVisible(!visible);
						}}>
						{" "}
						{visible ? <HiOutlineEyeSlash /> : <HiOutlineEye />}
					</button>
				</div>
				<div className='w-full'>
					<Button type='submit' loading={signInLoadingButton} classes='w-fit btn bg-green-400 border-green-400 mt-4' value='Se connecter' />
				</div>
			</form>
		</>
	);
};
const ContactsTable = ({ contacts }) => {
	return (
		<Table className='w-full sm:w-2/3 mx-auto border border-white/20'>
			<TableCaption>Liste des clients enregistrés sur tidycal</TableCaption>
			<TableHeader>
				<TableRow>
					<TableHead className='text-white/70'>name</TableHead>
					<TableHead className='text-white/70'> Email</TableHead>
					<TableHead className='text-white/70'> Tel</TableHead>
				</TableRow>
			</TableHeader>
			<TableBody>
				{contacts.length && contacts?.map((contact) => (
					<TableRow key={contact.name}>
						<TableCell className='font-medium'>{contact.name}</TableCell>
						<TableCell>
							<Link href={`mailto:${contact.email}`}>{contact.email}</Link>
						</TableCell>
						<TableCell>
							<Link href={`tel:${contact.tel}`}>{contact.tel}</Link>
						</TableCell>
						
					</TableRow>
				))}
			</TableBody>
			<TableFooter>
				<TableRow>
					<TableCell colSpan={2}>Total</TableCell>
					<TableCell className='text-right'>{contacts.length}</TableCell>
				</TableRow>
			</TableFooter>
		</Table>
	);
};
const Pages = () => {
	const { data: user } = useSession();
	const [contacts, setContacts] = useState([]);
	useEffect(() => {

		//filter function
		function uniqByKeepFirst(a, key) {
			let seen = new Set();
			return a.filter(item => {
				let k = key(item);
				return seen.has(k) ? false : seen.add(k);
			});
		}
		

		const getContacts = async () => {
			if (user) {
				toast.success('Connecté');
				const res = await fetch("/api/contacts",{ next: { revalidate: 0 }});
				if (res.ok) {
					const _data = await res.json();
					//  filter duplicate entries
					const data = uniqByKeepFirst(_data, it=>it.email);
					setContacts(data);
				} else {
					toast.error('Impossible de récupérer les contacts');
				}
			}
		};
		getContacts();
	}, [user]);

	const handleSignOut = () => {
		signOut({ callbackUrl: "/" });
	};
	return (
		<div className='min-h-screen flex flex-col justify-center items-center bg-primary text-white'>
			{user ? (
				<>
					<div>
						<Button
							value='Se deconnecter'
							onClick={handleSignOut}
							classes='absolute top-[calc(var(--mobile-nav-height)_+_10px)] sm:top-[calc(var(--desktop-nav-height)_+_10px)] right-2 w-fit btn bg-orange-400 justify-center text-xs !p-1.5'
						/>
					</div>
					<div className='w-full'>
						<ContactsTable contacts={contacts} />
					</div>
				</>
			) : (
				<div className='flex flex-col justify-start items-start'>
					<div className=''>
						<SignIn />
					</div>
				</div>
			)}
		</div>
	);
};

export default Pages;
