"use client";
import React from "react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { FaMousePointer } from "react-icons/fa";
import { MdRefresh} from "react-icons/md";
import { IoMdArrowForward,IoMdArrowBack } from "react-icons/io";
const NotFound = () => {
	const pathname = usePathname();
	return (
		<div className='min-h-screen grid place-items-center bg-grandient-to-l from-primary to-tertiary'>
			<div className='grid grid-rows-3 justify-center items-center gap-4 text-center'>
				<h1 className='text-4xl font-bold'>404 - Page introuvable</h1>
				<p className='text-xl'>Oups !! La page que vous cherchez n&apos;existe pas</p>
				<hr />
				<p className='text-xl'>Êtes vous sûr de L&apos;adresse :</p>
				<div className='relative'>
					<div className='bf-white flex flex-row justify-start w-full gap-2'>
						<div className='flex flex-row justify-evently items-center gap-2'>
							<IoMdArrowBack className='text-[#ABABAB]' />
							<IoMdArrowForward className='text-[#C1C1C1]' />
							<MdRefresh className='text-slate-400 bg-[#ECEFF7] text-[#ABABAB] p-2 rounded-full size-9 ' />
						</div>
						<p className='w-full text-lg italic text-[#474747] bg-[#ECEFF7] border pl-6 py-1 text-left  rounded-full'>https://...{decodeURI(pathname)}</p>
					</div>
					<FaMousePointer className='size-4 absolute right-6 top-2.5 animate-bounce' />
				</div>
				<hr />
				<Link href='/' className='btn bg-secondary mt-10'>
					Retourner sur la page d&apos;accueil
				</Link>
			</div>
		</div>
	);
};

export default NotFound;
