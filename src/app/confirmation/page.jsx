"use client"
import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { FaCheck } from "react-icons/fa";
import Confetti from 'react-confetti'
import useWindowSize from "react-use/lib/useWindowSize";
const Page = () => {
	const [run, setRun] = useState(true)
	const { width, height } = useWindowSize();
	
	useEffect(() => {
	setInterval(() => {
		setRun(false);
	}, 5000);
}, [])

	return (
		<section className='min-h-screen lg:px-24 sm:pt-[var(--desktop-nav-height)] pt-[var(--mobile-nav-height)] bg-gradient-to-l from-primary to-quarnary'>
			{width != "Infinity" && (
				<div className='px-5 sm:px-20 flex flex-col gap-10 pt-[var(--mobile-nav-height)] sm:pt-[var(--desktop-nav-height)] max-w-[980px] mx-auto'>
					<h1 className='animate-fade-in text-secondary'>Félicitation, Votre réservation a été prise en compte !!</h1>
					<Confetti width={width} height={height} recycle={run} />
					<div className='animate-slide-in my-5 border rounded p-8 bg-secondary shadow-xl'>
						<h2 className='text-quarnary'>Ce qu&apos;il vous reste à faire </h2>
						<p>
							<strong>
								Liste avec des check mark centrés <br /> Les prevenir qu&apos;ils vont recevoir un mail avec possibilité d&apos;ajouter l&apos;éveneement au calendar <br /> de verifier
								les spans
							</strong>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo eos possimus fugit nam totam dolore necessitatibus? Itaque perspiciatis nemo sunt eius doloremque quibusdam
							sit dolores quod, labore sed at nisi sint ab suscipit. Quibusdam magni vitae ad explicabo perferendis adipisci mollitia maiores dolore, soluta, quisquam, voluptatem nemo
							sit ea laborum expedita tempora dolores alias officia deleniti tempore est molestiae! Ea, sunt. Error, quis obcaecati dolore quae quaerat, pariatur recusandae, ex
							consectetur fugiat eos fugit quibusdam ad magnam? Ea cumque dignissimos cupiditate dicta? Labore optio debitis a fuga eveniet cupiditate facere porro quaerat provident
							autem. Aliquid repudiandae, autem temporibus natus ratione laboriosam, earum delectus, dolorem nisi impedit obcaecati. Facilis, necessitatibus? Aperiam sequi repellat ipsa
							eius atque, voluptate iusto! Tenetur sint, necessitatibus ex eum aspernatur labore veniam. Sint magni, quas blanditiis animi vitae dolorem minus praesentium, recusandae
							quae sequi, repudiandae earum aperiam accusantium doloremque?
						</p>
					</div>
					<div className='animate-slide-in my-5 border rounded p-8 bg-secondary shadow-xl'>
						<h2 className='text-quarnary'>Ce qu&apos;il vous reste à faire </h2>
						<p>
							<strong>
								Liste avec des check mark centrés <br /> Les prevenir qu&apos;ils vont recevoir un mail avec possibilité d&apos;ajouter l&apos;éveneement au calendar <br /> de verifier
								les spans
							</strong>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo eos possimus fugit nam totam dolore necessitatibus? Itaque perspiciatis nemo sunt eius doloremque quibusdam
							sit dolores quod, labore sed at nisi sint ab suscipit. Quibusdam magni vitae ad explicabo perferendis adipisci mollitia maiores dolore, soluta, quisquam, voluptatem nemo
							sit ea laborum expedita tempora dolores alias officia deleniti tempore est molestiae! Ea, sunt. Error, quis obcaecati dolore quae quaerat, pariatur recusandae, ex
							consectetur fugiat eos fugit quibusdam ad magnam? Ea cumque dignissimos cupiditate dicta? Labore optio debitis a fuga eveniet cupiditate facere porro quaerat provident
							autem. Aliquid repudiandae, autem temporibus natus ratione laboriosam, earum delectus, dolorem nisi impedit obcaecati. Facilis, necessitatibus? Aperiam sequi repellat ipsa
							eius atque, voluptate iusto! Tenetur sint, necessitatibus ex eum aspernatur labore veniam. Sint magni, quas blanditiis animi vitae dolorem minus praesentium, recusandae
							quae sequi, repudiandae earum aperiam accusantium doloremque?
						</p>
					</div>
					<div className='flex flex-row max-sm:flex-wrap gap-4 justify-center'>
						<div className='animate-slide-in my-5 border rounded p-8 bg-primary shadow-xl'>
							<h2 className='text-tertiary'>Si vous voulez me joindre ou m&apos;envoyer de document avant la séance </h2>
							<p className='text-white'>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo eos possimus fugit nam totam dolore necessitatibus? Itaque perspiciatis nemo sunt eius doloremque
								quibusdam sit dolores quod, labore sed at nisi sint ab suscipit. Quibusdam magni vitae ad explicabo perferendis adipisci mollitia maiores dolore, soluta, quisquam,
								voluptatem nemo sit ea laborum expedita tempora dolores alias officia deleniti tempore est molestiae! Ea, sunt. Error, quis obcaecati dolore quae quaerat, pariatur
								recusandae, ex consectetur fugiat eos fugit quibusdam ad magnam? Ea cumque dignissimos cupiditate dicta? Labore optio debitis a fuga eveniet cupiditate facere porro
								quaerat provident autem. Aliquid repudiandae, autem temporibus natus ratione laboriosam, earum delectus, dolorem nisi impedit obcaecati. Facilis, necessitatibus?
								Aperiam sequi repellat ipsa eius atque, voluptate iusto! Tenetur sint, necessitatibus ex eum aspernatur labore veniam. Sint magni, quas blanditiis animi vitae dolorem
								minus praesentium, recusandae quae sequi, repudiandae earum aperiam accusantium doloremque?
							</p>
						</div>
						<div className='animate-slide-in my-5 border rounded p-8 bg-quarnary shadow-xl'>
							<h2 className='text-secondary'>Comment vous préparer </h2>
							<p className='text-white'>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo eos possimus fugit nam totam dolore necessitatibus? Itaque perspiciatis nemo sunt eius doloremque
								quibusdam sit dolores quod, labore sed at nisi sint ab suscipit. Quibusdam magni vitae ad explicabo perferendis adipisci mollitia maiores dolore, soluta, quisquam,
								voluptatem nemo sit ea laborum expedita tempora dolores alias officia deleniti tempore est molestiae! Ea, sunt. Error, quis obcaecati dolore quae quaerat, pariatur
								recusandae, ex consectetur fugiat eos fugit quibusdam ad magnam? Ea cumque dignissimos cupiditate dicta? Labore optio debitis a fuga eveniet cupiditate facere porro
								quaerat provident autem. Aliquid repudiandae, autem temporibus natus ratione laboriosam, earum delectus, dolorem nisi impedit obcaecati. Facilis, necessitatibus?
								Aperiam sequi repellat ipsa eius atque, voluptate iusto! Tenetur sint, necessitatibus ex eum aspernatur labore veniam. Sint magni, quas blanditiis animi vitae dolorem
								minus praesentium, recusandae quae sequi, repudiandae earum aperiam accusantium doloremque?
							</p>
						</div>
					</div>
					<div className='flex flex-row max-sm:flex-wrap gap-4 justify-evenly py-4'>
						<Link href='/' className='btn block bg-secondary' type='button'>
							Page d&apos;accueil{" "}
						</Link>
						<Link href='/consultation' type='button' className='btn block bg-secondary'>
							Prendre un autre rendez-vous{" "}
						</Link>
					</div>
				</div>
			)}
		</section>
	);
}

export default Page