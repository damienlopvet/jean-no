import React from "react";
import Link from "next/link";
import { FaTiktok, FaYoutube, FaFacebook, FaInstagram, FaWhatsapp, FaVideo } from "react-icons/fa";
import { MdMailOutline } from "react-icons/md";
const Footer = () => {
	return (
		<div className='bg-primary '>
			<div className='text-white max-w-[980px] mx-auto'>
				<div className='hidden sm:flex flex-row justify-around gap-8 py-8'>
					<ul className='px-10'>
						<li>
							<h3 className='text-base'>
								<Link href='/'>Social Media</Link>
							</h3>
						</li>
						<li className='grid place-items-left'>
							<Link href='https://www.tiktok.com/@jeannoelmedium' className='flex flex-row gap-2 items-center'>
								<FaTiktok /> <span>Tiktok</span>
							</Link>
						</li>
						<li className='grid place-items-left'>
							<Link href='https://www.youtube.com' className='flex flex-row gap-2 items-center'>
								<FaYoutube />
								<span>Youtube</span>
							</Link>
						</li>
						<li className='grid place-items-left'>
							<Link href='https://www.facebook.com' className='flex flex-row gap-2 items-center'>
								<FaFacebook />
								<span>Facebook</span>
							</Link>
						</li>
						<li className='grid place-items-left'>
							<Link href='https://www.instagram.com' className='flex flex-row gap-2 items-center'>
								<FaInstagram />
								<span>Instagram</span>
							</Link>
						</li>
					</ul>
					<div className='w-[1px] bg-tertiary'></div>
					<ul className='px-10'>
						<li>
							<h3 className='text-base'>
								<Link href='/'>Contact</Link>
							</h3>
						</li>
						<li className='grid place-items-left'>
							<Link href='https://wa.me/+66932323113' className='flex flex-row gap-2 items-center'>
								<FaWhatsapp /> <span>Whatsapp</span>
							</Link>
						</li>
						<li className='grid place-items-left'>
							<Link href='mailto:kRJpJ@example.com' className='flex flex-row gap-2 items-center'>
								<MdMailOutline /> <span>mail</span>
							</Link>
						</li>
						<li className='grid place-items-left'>
							<Link href='facetime-audio:+66932323113' className='flex flex-row gap-2 items-center'>
								<FaVideo /> <span>FaceTime</span>
							</Link>
						</li>
					</ul>
					<div className='w-[1px] bg-tertiary'></div>
					<ul className='px-10'>
						<li>
							<h3 className='text-base'>
								<Link href='/'>PLan du site</Link>
							</h3>
						</li>
						<li>
							<Link href='/'>Accueil</Link>
						</li>
						<li>
							<Link href='/a-propos-de-jean-noel'>À propos</Link>
						</li>
						<li>
							<Link href='/consultation'>Consultation</Link>
						</li>
						<li>
							<Link href='/contact'>contact</Link>
						</li>
					</ul>
				</div>
				<div className='w-full flex flex-wrap gap-8 justify-center items-center sm:border-t border-tertiary/30  py-2 text-sm sm:text-xs'>
					<p>© 2024 Jean-Noël. Tous droits réservés.</p>
					<p>
						Powered by{" "}
						<Link href='https://lopvet-damien.com' target='blank'>
							@Damien
						</Link>
					</p>
				</div>
			</div>
		</div>
	);
};

export default Footer;
