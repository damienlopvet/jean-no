"use client";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";
import Brand from "@/src/components/Brand";
import { GiGalaxy } from "react-icons/gi";
import { FaHome, FaTiktok, FaInstagram } from "react-icons/fa";
import { ImProfile } from "react-icons/im";
import { BiUser } from "react-icons/bi";
import { BsFillInfoCircleFill } from "react-icons/bs";
import { FaWhatsapp, FaPhone, FaGoogle } from "react-icons/fa";
import { MdEmail } from "react-icons/md";

import { Menubar, MenubarMenu, MenubarTrigger, MenubarContent } from "@/src/components/ui/menubar";

const Nav = () => {
	const [prevOffset, setPrevOffset] = useState(0);
	const [visible, setVisible] = useState(true);

	useEffect(() => {
		const handleScroll = () => {
			const _offset = window.scrollY;
			setVisible(prevOffset > _offset || prevOffset < 10);
			setPrevOffset(_offset);
		};
		const onScroll = () => handleScroll();

		window.addEventListener("scroll", onScroll);

		return () => window.removeEventListener("scroll", onScroll);
	}, [prevOffset]);

	const navItems = () => {
		return (
			<div className='max-w-[980px] mx-auto'>
				<Menubar className='max-sm:bg-gradient-to-l max-sm:from-primary max-sm:to-quarnary rounded-none shadow-xl border-0 justify-center bg-transparent sm:justify-end h-[var(--mobile-nav-height)] sm:h-[var(--desktop-nav-height)] sm:pr-10 sm:space-x-1'>
					<MenubarMenu>
						<Link href='/' className='sm:mr-auto hidden sm:block'>
							<MenubarTrigger className='space-x-4 hover:bg-transparent data-[highlighted]:bg-transparent data-[state=open]:bg-transparent'>
								<div className='ml-10'>
									<Brand screen={"desktop"} />
								</div>
							</MenubarTrigger>
						</Link>
					</MenubarMenu>
					<MenubarMenu>
						<Link href='/'>
							<MenubarTrigger className='text-white uppercase font-bold text-xs  max-[300px]:px-1'>
								<div className='flex flex-col justify-center gap-2 items-center'>
									<FaHome className='sm:hidden h-5 w-5' />
									<span>Accueil</span>
								</div>
							</MenubarTrigger>
						</Link>
					</MenubarMenu>
					<MenubarMenu>
						<Link href='/a-propos-de-jean-noel'>
							<MenubarTrigger className='text-white uppercase font-bold text-xs  whitespace-nowrap max-[300px]:px-1'>
								<div className='flex flex-col justify-center gap-2 items-center'>
									<ImProfile className='sm:hidden h-5 w-5' />
									<span>À propos</span>
								</div>
							</MenubarTrigger>
						</Link>
					</MenubarMenu>
					<MenubarMenu>
						<Link href='/consultation'>
							<MenubarTrigger className='text-white uppercase font-bold text-xs  max-[300px]:px-1'>
								<div className='flex flex-col justify-center gap-2 items-center'>
									<div className='flex flex-row sm:hidden'>
										<BiUser className='h-5 w-5' />
										<BiUser className='h-5 w-5 -translate-x-1' />
									</div>
									<span>Consultation</span>
								</div>
							</MenubarTrigger>
						</Link>
					</MenubarMenu>

					<MenubarMenu>
						<MenubarTrigger className='text-white uppercase font-bold text-xs  max-[300px]:px-1'>
							<div className='flex flex-col justify-center gap-2 items-center'>
								<BsFillInfoCircleFill className='sm:hidden h-5 w-5' />
								<span>Contact</span>
							</div>
						</MenubarTrigger>
						<MenubarContent className='bg-white/50 border-0 backdrop-blur-sm rounded-none'>
							<div className='p-2 gap-2  flex flex-col justify-center items-center w-full '>
								<div className='w-full text-xs'>
									<Link href='https://wa.me/+66932323113' className='btn bg-[#1daa61] flex items-center justify-center flex-row gap-3 w-full' target='blank'>
										<FaWhatsapp className='size-5' />
										whatsapp
									</Link>
								</div>
								<div className='w-full text-xs'>
									<Link href='mailto:mail@lopvet-damien.com' className=' btn bg-[#c5221f] flex items-center justify-center flex-row gap-3 w-full' target='blank'>
										<MdEmail className='size-5' />
										email
									</Link>
								</div>
								<div className='w-full text-xs'>
									<Link href='tel:+41782519580' className=' btn bg-secondary flex items-center justify-center flex-row gap-3 w-full' target='blank'>
										<FaPhone className='size-5 rotate-[110deg]' />
										+66 93 232 3113
									</Link>
								</div>
								<div className='w-full text-xs'>
									<Link href='https://www.tiktok.com/@jeannoelmedium' className='btn bg-slate-800 flex items-center justify-center flex-row gap-3 w-full !lowercase' target='blank'>
										<FaTiktok className='size-5' />
										@jeannoelmedium
									</Link>
								</div>
								<div className='w-full text-xs'>
									<Link
										href='https://www.instagram.com/jeannoelmedium/'
										className='btn bg-[radial-gradient(circle_at_30%_107%,_#fdf497_0%,_#fdf497_5%,_#fd5949_45%,#d6249f_60%,#285AEB_90%)] flex items-center justify-center flex-row gap-3 w-full !lowercase'
										target='blank'>
										<FaInstagram className='size-5' />
										#jeannoelmedium
									</Link>
								</div>
							</div>
						</MenubarContent>
					</MenubarMenu>
				</Menubar>
			</div>
		);
	};

	return (
		<>
			{/* desktop */}
			<nav
				className={`${
					visible ? "translate-y-0" : "-translate-y-[var(--desktop-nav-height)]"
				} bg-gradient-to-l from-primary to-quarnary hidden overflow-hidden sm:block w-full h-[var(--desktop-nav-height)] fixed bottom-0 top-0 z-50 transition-all duration-500 ease-in-out`}>
				{navItems()}
			</nav>
			{/* mobile */}
			{/* top */}
			<nav
				className={`${visible ? "translate-y-0" : "-translate-y-[var(--mobile-nav-height)]"} overflow-hidden
			 sm:hidden w-full h-[var(--mobile-nav-height)] fixed bottom-0 top-0 z-50 transition-all duration-300 ease-in-out  bg-gradient-to-l from-primary to-quarnary justify-center`}>
				<Link className='mr-auto gap-4 flex flex-row p-2 w-full h-full justify-center items-center ' href='/'>
					<div className='mx-auto'>
						<Brand screen={"mobile"} />
					</div>
				</Link>
			</nav>
			{/* bottom */}
			<nav
				className={`${
					visible ? "translate-y-0" : "translate-y-[var(--mobile-nav-height)]"
				} sm:hidden w-full h-[var(--mobile-nav-height) fixed bottom-0 z-50 transition-all duration-500 ease-in-out`}>
				{navItems()}
			</nav>
		</>
	);
};

export default Nav;
