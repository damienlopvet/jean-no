"use client"
import React from "react";
import Image from "next/image";
import Link from "next/link";
import Script from "next/script";
import { BiUser } from "react-icons/bi";
import { BsClockHistory } from "react-icons/bs";
import { CgScrollH } from "react-icons/cg";
import { FaCreditCard } from "react-icons/fa";
const HomePage = () => {
	
	const ConsultCard = ({ title, duration, description, bookingType, price }) => {
		return (
			<div className='pb-8 h-8/12 w-5/12 min-w-[240px] max-w-[340px] relative bg-primary flex flex-col justify-start gap-6 shrink-0 rounded overflow-hidden shadow-xl'>
				<Image src='/hero.jpeg' alt='image of a face in the space' height={200} width={250} style={{ objectFit: "cover", objectPosition: "center", height: "200px", width: "auto" }} />
				<div className='px-6 flex flex-col justify-start gap-4 text-white'>
					<h3 className='animate-fade-in no-wrap text-secondary h-12'>{title}</h3>
					<div className='flex flex-row gap-4 items-center  '>
						<BsClockHistory className='size-5' />
						<p>{duration}</p>
					</div>
					<p>{description}</p>
					<div className='flex flex-row gap-4 items-center  '>
						<FaCreditCard  className='size-5' />
						<p className="text-lg">{price}</p> <span>€ / CHF</span>
					</div>
				</div>
				<div className="px-6">
					<Link className='block btn w-full text-center bg-tertiary' href={`/consultation#${bookingType}`}>
						{" "}
						Voir les détails
					</Link>
				</div>
			</div>
		);
	};

	return (
		<div className='bg-tertiary'>
			<div className='w-full relative'>
				<div className='bg-secondary'>
					<Image src='/hero.jpeg' width='1920' height='1080' alt='image of a face in the space' className='w-full h-screen' style={{ objectFit: "cover", objectPosition: "center" }} />
				</div>
				<div className='space-y-2 animate-fade-in backdrop-blur-sm bg-black/10 absolute h-fit w-[80%] sm:max-w-[980px] top-[60%] sm:top-1/2 left-1/2 sm:left-1/2 -translate-y-1/2 -translate-x-1/2 text-white bg-black/30 p-2 sm:p-4'>
					<h1 className=' uppercase max-[500px]:my-0 max-sm:my-2 sm:my-5 no-wrap text-3xl whitespace-nowrap drop-shadow-xl tracking-wider'>Jean-Noël</h1>
					<h2 className='text-xl whitespace-nowrap tracking-wider text-white font-normal drop-shadow-xl '>
						Medium - <wbr />
						France | Suisse
					</h2>
					<p className=' max-sm:pt-0 pt-3 font-semibold text-base '>
						a concise and impactful tagline or mission statement about Jean-Noëla concise and impactful tagline or mission statement about Jean-Noël{" "}
					</p>
					<Link type='button' aria-label='consultation' className='block w-fit btn bg-primary text-sm text-center' href='/consultation'>
						Prendre <wbr />
						rendez-vous
					</Link>
				</div>
			</div>
			{/* <div className='lg:hidden flex flex-col justify-center items-center gap-4 p-8 text-lg text-center bg-primary'>
				<span className='uppercase text-xs font-semibold'>Présentation</span>
				<h1 className='animate-fade-in uppercase mb-5 text-secondary'>Jean-Noël</h1>
				<q className='animate-fade-in'>a concise and impactful tagline or mission statement.</q>
				<Link type='button' aria-label='a propos' className='btn w-fit' href='/consultation'>
					Prendre rendez-vous
				</Link>
			</div> */}
			{/* <div className='spacer layer1 relative inset-0 scale-y-[1.1] rotate-[180deg] lg:hidden'></div> */}

			{/* MARK:three card */}
			<div className='spacer layer3 relative inset-0 scale-y-[1.1] rotate-[180deg] -translate-y-1/2 lg:hidden'></div>

			<section className='max-sm:px-4 max-w-[980px] mx-auto lg:translate-y-full lg:slide-in-y grid lg:grid-cols-3 max-lg:grid-rows-3 gap-6 items-center justify-center max-lg:py-10 bg-tertiary'>
				<div className='max-lg:fade-out bg-stone-50 lg:-translate-y-1/2 border rounded bg-white shadow-xl p-6'>
					<div className='flex flex-row flex-wrap max-sm:flex-col items-center gap-4 pb-4'>
						<div className='flex flex-row'>
							<BiUser className='h-5 w-5' />
						</div>
						<h2 className='whitespace-nowrap font-normal text-xl text-primary'>type de medium I</h2>
					</div>
					<p className='font-normal max-sm:text-center'>main services or areas of expertise offered by the consultant..</p>
				</div>
				<div className='max-lg:fade-out bg-stone-50 lg:-translate-y-1/2 border rounded bg-white shadow-xl p-6'>
					<div className='flex flex-row flex-wrap max-sm:flex-col items-center gap-4 pb-4'>
						<BiUser className='h-5 w-5' />
						<h2 className='whitespace-nowrap font-normal text-xl text-primary'>Type de medium II</h2>
					</div>
					<p className='font-normal max-sm:text-center'>main services or areas of expertise offered by the consultant..</p>
				</div>
				<div className='max-lg:fade-out bg-stone-50 lg:-translate-y-1/2 border rounded bg-white shadow-xl p-6'>
					<div className='flex flex-row flex-wrap max-sm:flex-col items-center gap-4 pb-4'>
						<BiUser className='h-5 w-5' />
						<h2 className='whitespace-nowrap font-normal text-xl text-primary'>type de medium III</h2>
					</div>
					<p className='font-normal max-sm:text-center'>main services or areas of expertise offered by the consultant..</p>
				</div>
			</section>
			<div className='spacer greenRed relative inset-0 scale-y-[1.1] bg-kaki'></div>

			{/* MARK:Un medium pas comme les autres */}
			<section className=' p-10 sm:px-24  bg-quarnary'>
				<div className='space-y-8 fade-out  max-w-[980px] mx-auto'>
					<div className='text-white'>
						<h2 className='text-secondary'>Un medium pas comme les autres</h2>
						<p>
							<strong>une présentation succinte</strong>
						</p>
					</div>
					<p className='text-white'>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi ipsum sunt quia quisquam quod, perspiciatis ratione delectus eaque, exercitationem harum beatae libero totam
						veritatis est? Dignissimos debitis laboriosam quo voluptate esse et dicta beatae distinctio dolorum cum. Perferendis expedita, atque ea ab qui nam doloribus quisquam
						perspiciatis. Beatae quod atque ex sunt id eum est voluptatum! Maiores unde quisquam provident consequatur nemo officia. Totam, omnis necessitatibus. Qui odio consectetur
						veniam saepe aliquid. Eveniet repellat officiis minus veritatis recusandae odit assumenda, quia suscipit saepe reiciendis. Tempora possimus facilis aut molestiae nulla quas
						mollitia perspiciatis ipsum commodi quia ad nostrum hic fuga
					</p>
					<Link href='/a-propos-de-jean-noel' className='btn block w-fit text-white bg-secondary'>
						En savoir plus
					</Link>
				</div>
			</section>
			<div className='spacer yellowRed relative inset-0 rotate-[180deg] scale-y-[1.1] bg-secondary'></div>

			{/* consultCard */}
			<section className='py-10 px-4 sm:px-24 space-y-8 bg-secondary'>
				<div className=' max-w-[980px] mx-auto'>
					<h2 className='text-primary'>
						Consultez votre <strong>medium</strong>
					</h2>
				</div>
				<div className='flex flex-row gap-8 overflow-x-scroll py-8 no-scrollbar  max-w-[980px] mx-auto'>
					<ConsultCard
						title={"Mediumité de contact"}
						price={60}
						duration={"60 min"}
						description={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  "}
						bookingType={"contact"}
					/>
					<ConsultCard
						title={"Évaluation d'habitation"}
						duration={"120 min"}
						description={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  "}
						bookingType={"habitation"}
						price={60}
					/>
					<ConsultCard
						title={"Coaching déprogrammation"}
						duration={"90 min"}
						description={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  "}
						bookingType={"coaching"}
						price={60}
					/>
					<ConsultCard
						title={"Mediumité de contact"}
						duration={"60 min"}
						description={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  "}
						bookingType={"contact"}
						price={"80"}
					/>
				</div>
				<div className=' grid 2xl:hidden place-items-center'>
					<CgScrollH className='size-8 text-slate-800' />
				</div>
			</section>
			<div className='spacer layer4 relative inset-0 scale-y-[1.1] bg-secondary '></div>

			<section className='py-10 px-4 sm:px-24 space-y-8'>
				<div className='max-w-[980px] mx-auto'>
					<h2 className='text-primary'>Fil d&apos;actualité</h2>
				</div>
				<blockquote
					className='tiktok-embed'
					cite='https://www.tiktok.com/@jeannoelmedium'
					data-unique-id='jeannoelmedium'
					data-embed-type='creator'
					style={{ maxWidth: "980px", minWidth: "200px", borderRadius: "3px" }}>
					{" "}
					<section>
						{" "}
						<a target='_blank' href='https://www.tiktok.com/@jeannoelmedium?refer=creator_embed'>
							@jeannoelmedium
						</a>{" "}
					</section>{" "}
				</blockquote>{" "}
				<Script async src='https://www.tiktok.com/embed.js'></Script>
				{/* MARK:Instagram */}
				<div className='flex justify-center items-center w-full mx-auto max-w-[980px]'>
					<blockquote
						className='instagram-media'
						data-instgrm-permalink='https://www.instagram.com/jeannoelmedium/'
						data-instgrm-version='14'
						style={{
							background: "#FFF",
							border: "0",
							borderRadius: "3px",
							boxShadow: "0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15)",
							margin: "1px",
							maxWidth: "980px",
							minWidth: "200px",
							padding: "0",
							width: "99.375%",
							width: "webkit-calc(100% - 2px)",
							width: "calc(100% - 2px)",
							borderRadius: "20px",
						}}>
						{/* <div style={{ padding: "16px" }}>
							{" "}
							<Link
								href='https://www.instagram.com/p/C6WE5nLNDMa/?utm_source=ig_embed&amp;utm_campaign=loading'
								style={{ background: "#FFFFFF", lineHeight: "0", padding: "0 0", textAlign: "center", textDecoration: "none", width: "100%" }}
								target='_blank'>
								{" "}
								<div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
									{" "}
									<div style={{ backgroundColor: "#F4F4F4", borderRadius: "50", flexGrow: "0", height: "40px", marginRight: "14px", width: "40px" }}></div>{" "}
									<div style={{ display: "flex", flexDirection: "column", flexGrow: "1", justifyContent: "center" }}>
										{" "}
										<div style={{ backgroundColor: "#F4F4F4", borderRadius: "4px", flexGrow: "0", height: "14px", marginBottom: "6px", width: "100px" }}></div>{" "}
										<div style={{ backgroundColor: "#F4F4F4", borderRadius: "4px", flexGrow: "0", height: "14px", width: "60px" }}></div>
									</div>
								</div>
								<div style={{ padding: "19% 0" }}></div>{" "}
								<div style={{ display: "block", height: "50px", margin: "0 auto 12px", width: "50px" }}>
									<svg width='50px' height='50px' viewBox='0 0 60 60' version='1.1' xmlns='https://www.w3.org/2000/svg' xmlnsXlink='https://www.w3.org/1999/xlink'>
										<g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'>
											<g transform='translate(-511.000000, -20.000000)' fill='#000000'>
												<g>
													<path d='M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631'></path>
												</g>
											</g>
										</g>
									</svg>
								</div>
								<div style={{ paddingTop: "8px" }}>
									{" "}
									<div style={{ color: "#3897f0", fontFamily: "Arial,sans-serif", fontSize: "14px", fontStyle: "normal", fontWeight: "550", lineHeight: "18px" }}>
										Voir cette publication sur Instagram
									</div>
								</div>
								<div style={{ padding: "12.5% 0" }}></div>{" "}
								<div style={{ display: "flex", flexDirection: "row", marginBottom: "14px", alignItems: "center" }}>
									<div>
										{" "}
										<div style={{ backgroundColor: "#F4F4F4", borderRadius: "50%", height: "12.5px", width: "12.5px", transform: "translateX(0px) translateY(7px)" }}></div>{" "}
										<div
											style={{
												backgroundColor: "#F4F4F4",
												height: "(12.5px)",
												transform: "rotate(-45deg) translateX(3px) translateY(1px)",
												width: "12.5px",
												flexGrow: "0",
												marginRight: "14px",
												marginLeft: "2px",
											}}></div>{" "}
										<div style={{ backgroundColor: "#F4F4F4", borderRadius: "50%", height: "12.5px", width: "12.5px", transform: "translateX(9px) translateY(-18px)" }}></div>
									</div>
									<div style={{ marginLeft: "8px" }}>
										{" "}
										<div style={{ backgroundColor: "#F4F4F4", borderRadius: "50%", flexGrow: "0", height: "20px", width: "20px" }}></div>{" "}
										<div
											style={{
												width: "0",
												height: "0",
												borderTop: "2px solid transparent",
												borderLeft: "6px solid #f4f4f4",
												borderBottom: "2px solid transparent",
												transform: "translateX(16px) translateY(-4px) rotate(30deg)",
											}}></div>
									</div>
									<div style={{ marginLeft: "auto" }}>
										{" "}
										<div style={{ width: "0px", borderTop: "8px solid #F4F4F4", bordeRight: "8px solid transparent", transform: "translateY(16px)" }}></div>{" "}
										<div style={{ backgroundColor: "#F4F4F4", flexGrow: "0", height: "12px", width: " 16px", transform: "translateY(-4px)" }}></div>{" "}
										<div
											style={{
												width: "0",
												height: "0",
												borderTop: "8px solid #F4F4F4",
												borderLeft: "8px solid transparent",
												transform: "translateY(-4px) translateX(8px)",
											}}></div>
									</div>
								</div>{" "}
								<div style={{ display: "flex", flexDirection: "column", flexGrow: "1", justifyContent: "center", marginBottom: "24px" }}>
									{" "}
									<div style={{ backgroundColor: "#F4F4F4", borderRadius: "4px", flexGrow: "0", height: "14px", marginBottom: "6px", width: "224px" }}></div>{" "}
									<div style={{ backgroundColor: "#F4F4F4", borderRadius: "4px", flexGrow: "0", height: "14px", width: "144px" }}></div>
								</div>
							</Link>
							<p
								style={{
									color: "#c9c8cd",
									fontFamily: "Arial,sans-serif",
									fontSize: "14px",
									lineHeight: "17px",
									marginBottom: "0",
									marginTop: "8px",
									overflow: "hidden",
									padding: "8px 0 7px",
									textAlign: "center",
									textOverflow: "ellipsis",
									whiteSpace: "nowrap",
								}}>
								<Link
									href='https://www.instagram.com/p/C6WE5nLNDMa/?utm_source=ig_embed&amp;utm_campaign=loading'
									style={{
										color: "#c9c8cd",
										fontFamily: "Arial,sans-serif",
										fontSize: "14px",
										fontStyle: "normal",
										fontWeight: "normal",
										lineHeight: "17px",
										textDecoration: "none",
									}}
									target='_blank'>
									Une publication partagée par Jean-Noël (@jeannoelmedium)
								</Link>
							</p>
						</div> */}
					</blockquote>
				</div>
				<Script async src='//www.instagram.com/embed.js'></Script>
			</section>
			<div className='spacer layer3 relative inset-0 scale-y-[1.1] bg-tertiary'></div>
		</div>
	);
};

export default HomePage;
