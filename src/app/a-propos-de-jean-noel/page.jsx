"use client"

import React from 'react'
import { FaRegUserCircle } from "react-icons/fa";
import Link from 'next/link'
import Image from "next/image";

const page = () => {
	return (
		<>
			<div className='w-full relative'>
				<Image
					src='/hero.jpeg'
					width='1920'
					height='1080'
					style={{ objectFit: "cover", objectPosition: "center" }}
					alt='image of a face in the space'
					className='relative z-10 md:h-[100svh] md:w-full'
				/>
				<div className='animate-fade-in absolute bottom-4 p-2 px-4 rounded backdrop-blur-sm bg-black/30 z-20 w-fit h-fit w-[80%] sm:max-w-[980px] left-1/2 -translate-x-1/2'>
					<h1 className='text-3xl sm:text-5xl md:text-6xl'>
						<span className='font-bold text-white whitespace-nowrap '>Jean-Noël</span> <br /> <span className='font-semibold text-white whitespace-nowrap'>Un trésor caché</span>
					</h1>
				</div>
			</div>
			<div className='spacer greenRed relative inset-0 scale-y-[1.1] z-[-1] bg-primary'></div>
			{/* MARK:  sticky cards */}
			<section className='px-5 sm:px-20 grid grid-rows-3 auto-rows-min gap-8 h-[calc(100%_+_100px)] pb-[7rem] bg-quarnary'>
				<div className=' max-w-[980px] mx-auto shadow-lg my-5 sticky shrink-up1 sm:top-[var(--desktop-nav-height)] top-[var(--mobile-nav-height)] bg-secondary p-8  rounded'>
					<h2 className='my-5 text-primary'>Jean-Noël, un trésor caché</h2>
					<p className='text-black text-xl'>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
						ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
						occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						<strong>a faire: confirm resa page; biographie; relier compte en banque; page contact; admin page view all clients </strong>
					</p>
					<div className='flex flex-row justify-start items-center gap-4 mt-10'>
						<FaRegUserCircle className='size-10 text-primary' />
						<p className='font-licorice text-3xl text-primary'>Named Person</p>
					</div>
				</div>
				<div className='max-w-[980px] mx-auto shadow-xl my-5 sticky shrink-up2 sm:top-[var(--desktop-nav-height)] top-[var(--mobile-nav-height)] bg-tertiary p-8  rounded'>
					<h2 className='my-5 text-secondary'>Un medium pas comme les autres</h2>
					<p className='text-black text-xl'>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
						ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
						occaecat
					</p>
				</div>
				<div className='max-w-[980px] mx-auto shadow-2xl my-5 sticky shrink-up3 sm:top-[var(--desktop-nav-height)] top-[var(--mobile-nav-height)] bg-primary p-8  rounded'>
					<h2 className='my-5 text-secondary'>Une pratique vraiment unique</h2>
					<p className='text-white text-xl'>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
						ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
						occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
						incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
						in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
						id est laborum.
					</p>
				</div>
			</section>
			<div className=' bg-quarnary relative w-full flex justify-center  scale-y-[1.1]'>
				<Link className='btn bg-primary text-white' href='/consultation'>
					{" "}
					Prendre rendez-vous
				</Link>
			</div>
			<div className='spacer redYellow relative inset-0 scale-y-[1.1] z-[-1] bg-quarnary'></div>
			{/* MARK: biographie */}
			<section className='space-y-10 bg-secondary px-10 pb-10'>
				<div className='flex flex-col justify-center items-center gap-8  max-w-[980px] mx-auto'>
					<FaRegUserCircle className='size-16 text-primary' />
					<h2 className='text-slate-800'>Biographie</h2>
				</div>
				<div className='space-y-4  max-w-[980px] mx-auto'>
					<h2 className='text-slate-800'>Son parcours</h2>
					<p
						className='border-l-4 first-letter:text-5xl first-letter:font-bold first-letter:text-quarnary
  first-letter:mr-1 first-letter:float-left border-primary pl-4 sm:pl-8 fade-out'>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas corrupti nostrum eius quis esse repellendus. Ipsa qui cum animi atque. Itaque provident unde non expedita
						minus labore beatae cumque vero quo praesentium, facilis nam repudiandae saepe nostrum, atque magni consequatur assumenda! Dolor asperiores omnis cum autem minus dolorum ipsam
						reprehenderit atque! Ducimus, provident! Ut illum enim molestias rerum, optio eum a alias nostrum tempore laudantium, nisi non repellendus possimus tempora. Accusamus earum
						praesentium ad autem illo beatae, aut quaerat nostrum sequi voluptatem veniam reprehenderit incidunt consequuntur. Hic, nam dolorem earum neque repellendus aperiam accusantium
						aut qui voluptates quis possimus voluptate officia sequi? Aliquam perferendis laudantium consequatur, alias nostrum animi maiores error maxime totam temporibus consectetur
						numquam, nihil, laborum odit similique quos iure omnis. Nemo earum recusandae exercitationem nulla minus incidunt deleniti qui ratione!
					</p>
				</div>
				<div className='space-y-4 max-w-[980px] mx-auto'>
					<h2 className='text-slate-800'>Son approche</h2>
					<p
						className='border-l-4 first-letter:text-5xl first-letter:font-bold first-letter:text-quarnary
  first-letter:mr-1 first-letter:float-left border-primary pl-4 sm:pl-8 fade-out'>
						Minus minima tenetur ipsam hic, sequi tempore fuga id consequatur, laborum aspernatur ad pariatur voluptas excepturi! Delectus obcaecati omnis laborum dolorum eligendi illum,
						molestiae necessitatibus dicta aliquam placeat! Perspiciatis voluptatum incidunt tempora dicta quo iure velit dolorem quia laboriosam laborum sit sed, earum quibusdam fuga
						dicta, odit et numquam quas expedita doloribus! Illum, voluptatem. Inventore dolore quo incidunt mollitia minima, iste corrupti. Perspiciatis quis pariatur at ipsa iure. Ad,
						recusandae sed consequatur impedit qui, ipsa voluptatum quaerat aperiam dolorum, fugit rerum eos quasi placeat aut libero iusto quod non? Ipsam ab cupiditate neque non
						laudantium deserunt ipsa dicta cum eius obcaecati. Eaque modi soluta architecto, aliquam officia cumque. Nostrum, perspiciatis eos? Rerum culpa corrupti voluptates placeat.
						Nobis, consequatur distinctio? Cumque ad nam quo doloremque illo, blanditiis dolor excepturi aperiam vero.
					</p>
				</div>
				<div className='space-y-4 max-w-[980px] mx-auto'>
					<h2 className='text-slate-800'>Sa construction</h2>
					<p
						className='border-l-4 first-letter:text-5xl first-letter:font-bold first-letter:text-quarnary
  first-letter:mr-1 first-letter:float-left border-primary pl-4 sm:pl-8'>
						Necessitatibus exercitationem delectus saepe maiores consequuntur ipsa autem dolores libero magni tempora fugiat, architecto, nam fuga, reiciendis at nulla mollitia quisquam
						suscipit distinctio neque laboriosam nisi natus consectetur voluptatibus! Expedita velit placeat eius numquam perspiciatis recusandae nulla sunt eaque nihil minus, illo
						voluptatum minima ullam fuga obcaecati veritatis tenetur? Voluptatum aspernatur eum odio, cum totam perspiciatis consectetur iure praesentium quo, eos quas? Ducimus unde at ab
						vel reiciendis vero minima ad facilis amet necessitatibus nostrum ipsa! Omnis esse animi non nobis dolores earum hic consequatur! Tempora non similique vitae atque odit! Iste
						temporibus corporis sint praesentium! Blanditiis voluptate maiores placeat harum culpa deleniti reprehenderit odio quae, eius, omnis rerum, iusto doloribus atque iure. Iure,
						quasi nostrum adipisci eum possimus corporis sequi accusamus inventore et in, commodi delectus modi voluptatum explicabo. Excepturi, dicta esse.
					</p>
				</div>
			</section>
			<div className='spacer kakiYellow relative inset-0  scale-y-[1.1] z-[-1] bg-secondary'></div>
		</>
	);
};

export default page