

ARG NODE_VERSION=20.2.0

FROM node:${NODE_VERSION}-alpine

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

ENTRYPOINT [""]
